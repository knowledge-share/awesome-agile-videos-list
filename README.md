A collaborative curated list of awesome Agile videos resources

## Contents

- [Agile Grenoble](#agile-grenoble)
	- [2019](#agile-grenoble-2019)
    - [2018](#agile-grenoble-2018)
    - [2017](#agile-grenoble-2017)
    - [2016](#agile-grenoble-2016)
- [Agile Montreal](#agile-montreal)
    - [Special](#agile-montreal-special)
	- [2019](#agile-montreal-2019)
    - [2018](#agile-montreal-2018)
    - [2017](#agile-montreal-2017)
    - [2016](#agile-montreal-2016)
    - [2015](#agile-montreal-2015)
    - [2014](#agile-montreal-2014)
    - [2013](#agile-montreal-2013)

## Agile Grenoble
### Agile Grenoble 2019
-  [Marie-Laure Brunet - S'il suffisait d'oser](https://www.youtube.com//watch?v=5umYW87SGwE&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=1&t=4s)
-  [Arnaud Lemaire - Et si on redémarrait l'agilité ?](https://www.youtube.com//watch?v=sZbmP0JZHBs&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=2&t=31s)
-  [Laurence Ruffin - Construire collectivement les décisions](https://www.youtube.com//watch?v=PyuCv04k_eg&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=3)
-  [Jean-Pierre Lambert - La collaboration, signe d'une véritable agilité](https://www.youtube.com//watch?v=JOy4zE3htAQ&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=4)
-  [Alexis & Isabel Monville - Les interactions entre les personnes](https://www.youtube.com//watch?v=J-YG9vNetWI&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=5)
-  [Arnaud Riou - Charisme & Fellowship du Manager](https://www.youtube.com//watch?v=1N04Nu0ON0Q&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=6)
-  [Anne Gabrillagues - Faire de l’amélioration continue une habitude quotidienne](https://www.youtube.com//watch?v=6RI967AgxA8&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=7)
-  [Aurelie Le Guillou - Les 20 questions qui font tilt lors de mes ateliers](https://www.youtube.com//watch?v=lZ9YkCE4GSc&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=8)
-  [David Desfougeres - La spirale agile](https://www.youtube.com//watch?v=2HBr3UsWmCQ&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=9)
-  [David Laizé - Si le babyfoot ça ne marche pas, comment je motive mon équipe ?](https://www.youtube.com//watch?v=aXxLWlAqSP0&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=10)
-  [Olivier Lequeux - La CNV dans la vraie vie !](https://www.youtube.com//watch?v=WoFbPPvQI54&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=11)
-  [Hélène Morre & Belkacem Settouti - Convaincre au delà des arguments](https://www.youtube.com//watch?v=qDNb6ol0_Qg&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=12)
-  [Jérome Bourgeon - Mindset, c'est quoi ce truc ?](https://www.youtube.com//watch?v=XbJQSxX0D8Y&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=13)
-  [Matthew Richter - Mythes, idées fausses, et poudre de perlimpinpin](https://www.youtube.com//watch?v=q6ZjD0owgsU&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=14)
-  [Maxime Bonnet - Debrief #Shiftup by Jurgen Appelo](https://www.youtube.com//watch?v=cS19Wt7ozXI&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=15)
-  [Nicolas Ploquin - Et si nos managers étaient indispensables ?](https://www.youtube.com//watch?v=BTDCE7GdWJc&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=16)
-  [Pascale Blanchard - SW!TCH](https://www.youtube.com//watch?v=au7QFb1HM5M&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=17)
-  [Romain Couturier - Ma vie est un ticket !](https://www.youtube.com//watch?v=XktCoj1gVR4&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=18)
-  [Ségolène Porot - Comment donner et recevoir un feedback ?](https://www.youtube.com//watch?v=H9kJXdr-IO4&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=19)
-  [Thomas di Luccio - Concevoir pour des futurs désirables](https://www.youtube.com//watch?v=-hcjgTZsMHQ&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=20)
-  [Vanessa Humphreys - L'innovation managériale](https://www.youtube.com//watch?v=fBubirSj2TE&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=21)
-  [Contraines et jeux sociaux pour des dojos au top](https://www.youtube.com//watch?v=y8uUu5rXi44&list=PL5ZgpmlSWfQslTVRDVM8XCeW5bjSSFD60&index=23)

### Agile Grenoble 2018
-  [Vanessa Humphreys - Help! Ma mère est un coach Agile](https://www.youtube.com//watch?v=ihw-2uZMe0c&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=1&t=3s)
-  [Alexis Monville - Changing Your Team From The Inside](https://www.youtube.com//watch?v=_afzdcxPsZk&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=2)
-  [Alexandre Boutin - Les Funérailles des méthode Agiles](https://www.youtube.com//watch?v=Xlc0kWVn5yw&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=3)
-  [G.Alexandre & I.Gauthier - Pour reussir vos ateliers, devenez facilitateur](https://www.youtube.com//watch?v=QEhfiZyDM4Y&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=4)
-  [Steeve Evers - Le Projet Aristote](https://www.youtube.com//watch?v=nk3u414WM-E&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=5)
-  [Vanessa Humphreys - Si à 40 ans on est pas manager c'est qu'on a raté sa vie](https://www.youtube.com//watch?v=jpjbt-E1EtU&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=6)
-  [Aurélien Morvant - Les 10 secrets pour bien (re)démarrer dans la agilité](https://www.youtube.com//watch?v=1EXdr6kUkIw&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=7)
-  [T.Gibot- Comment l'agilité m'a permis de terminer l'une des courses les plus dures du monde](https://www.youtube.com//watch?v=KyhlE6Qz49U&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=8)
-  [R. Couturier - Apprendre à se poser des questions  sur son Kanban](https://www.youtube.com//watch?v=ddbXp1r3E8M&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=9)
-  [A. Gabrillagues -Développez L'intelligence collective de vos équipes](https://www.youtube.com//watch?v=hq1KNmLBxtI&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=10)
-  [Keynote Jean Christophe Barralis - Appreciative Inquiry](https://www.youtube.com//watch?v=pnNhk73K9PY&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=11)
-  [Keynote Emmanuelle Le Bris & Philippe Chirade - Intelligence Émotionnelle, Comment développer son Agilité ?](https://www.youtube.com//watch?v=QCu_AQf05c0&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=13)
-  [Keynote Matthew Richter & Marion Ferlin - Politique, politique, politique - Gérer vos relations professionnelles au travail.](https://www.youtube.com//watch?v=cpZAvXQY94I&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=14)
-  [Aurélien Morvant -Facilitation Graphique Instantanée](https://www.youtube.com//watch?v=TaLkBg9NYxs&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=15)
-  [Keynote Franck Rageade - Le Fabuleux Destin de Tim Member](https://www.youtube.com//watch?v=L9-0Z9ghXmo&list=PL5ZgpmlSWfQuofRVpek15ysOjAMAHzGdX&index=16)

### Agile Grenoble 2017
-  [ Keynote Aurelien Morvant - "Transformations d'entreprise : Etes-vous prêts pour le grand saut"](https://www.youtube.com//watch?v=_4bM67KiXr0&list=PL5ZgpmlSWfQvtlPI_Hb0IbozBY5_thUlT&index=2)
-  [ Keynote Stephane Bigeard - Succès - Échec, ce que je regarde se développe](https://www.youtube.com//watch?v=e5qynVFuWLo&list=PL5ZgpmlSWfQvtlPI_Hb0IbozBY5_thUlT&index=3)
-  [ Keynote Pablo Pernot - Etre Agile](https://www.youtube.com//watch?v=pjH0x21pYA0&list=PL5ZgpmlSWfQvtlPI_Hb0IbozBY5_thUlT&index=4)

### Agile Grenoble 2016
-  [Christian Den Hartigh - Biomimétisme, agilité et pédagogie](https://www.youtube.com//watch?v=SL4lZd6lQrI&list=PL5ZgpmlSWfQtNskGikzUyvGLPpViuGeVX&index=2)
-  [Alexandre Boutin - Back 2 basics, les 10 principes de la rétrospectives](https://www.youtube.com//watch?v=000vmTr0vRA&list=PL5ZgpmlSWfQtNskGikzUyvGLPpViuGeVX&index=3)
-  [Gilles Namur & Sébastien Goodwin - On a fait bouger le mammouth avec une Obeya !](https://www.youtube.com//watch?v=kEhk2VL0BG0&list=PL5ZgpmlSWfQtNskGikzUyvGLPpViuGeVX&index=4)
-  [Andrea Provaglio - "The beating heart of Agile" ](https://www.youtube.com//watch?v=NFDj_vnhDoA&list=PL5ZgpmlSWfQtNskGikzUyvGLPpViuGeVX&index=6)
-  [Karim Benameur - "Trouillométrie : une nouvelle echelle de l'agilité"](https://www.youtube.com//watch?v=jqknRBtxO-E&list=PL5ZgpmlSWfQtNskGikzUyvGLPpViuGeVX&index=7)
-  [Franck Rageade - "Le Lab Oratoire du Dr. Frankenstein, ou comment (re)donner vie à la Créature Agile"](https://www.youtube.com//watch?v=qHijdFn8vBg&list=PL5ZgpmlSWfQtNskGikzUyvGLPpViuGeVX&index=8)

## Agile Montreal
### Agile Montreal special
-  [David Hussman - Making the move to a product culture](https://www.youtube.com//watch?v=Ea5flopSTvA&list=PLKUt9pKJ0E0e8BHic7NvqraJ79h9y-kKp&index=1)
-  [Martin Goyette - Introduction à l'agilité](https://www.youtube.com//watch?v=2ZnxePTazXE&list=PLKUt9pKJ0E0e8BHic7NvqraJ79h9y-kKp&index=2)
-  [Anti-Conférence de l’Agilité Organisationnelle](https://www.youtube.com//watch?v=Ocv1clMPjD4&list=PLKUt9pKJ0E0e8BHic7NvqraJ79h9y-kKp&index=3)
-  [Bob Martin - The Future of Agile](https://www.youtube.com//watch?v=FedQ2NlgxMI&list=PLKUt9pKJ0E0e8BHic7NvqraJ79h9y-kKp&index=4)

### Agile Montreal 2019
-  [Daniel Tardif - Why Agile Tour?](https://www.youtube.com//watch?v=kSOd0-AYlqM&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=2)
-  [Isabelle Therrien - La prise de notes visuelles (Sketchnoting), un outil d'apprentissage](https://www.youtube.com//watch?v=0dD4f2LGmQM&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=3)
-  [Daniel Tardif - Stop Abandoning, Start Delegating - What Makes a Strong Servant Leader](https://www.youtube.com//watch?v=-TtCRIZO8Ds&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=4)
-  [Mariana Vielmas, Janaki St-Pierre - De l’agilité et des femmes](https://www.youtube.com//watch?v=3J0X-e_1UX4&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=5)
-  [David Sabine - Stop Using Projects to Make Products](https://www.youtube.com//watch?v=EXRoJkcF2So&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=6)
-  [Céline Raguette - Le monde change, et vous ? Manifeste pour des RH agiles](https://www.youtube.com//watch?v=9PKJRODe2i4&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=7)
-  [Sandy Mamoli (Keynote) - How Self-Selection Lets People Excel](https://www.youtube.com//watch?v=yGFq7fmCjFs&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=8)
-  [Eric Lessard, Guillaume Dupuis - DevOps: quand les outils ne suffisent pas](https://www.youtube.com//watch?v=JC3fZ_bznBA&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=9)
-  [Michel Céré, Geneviève Giguère - Votre contrat est-il Agile](https://www.youtube.com//watch?v=eJEK6UQhWXA&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=10)
-  [Julien Jarles, Ariane Bisson - L’agilité d’affaires et si on changeait d’approche organisationnelle](https://www.youtube.com//watch?v=FTY9WxyZJPA&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=11)
-  [Jean-Christophe Schaffner, Orzu Kamolova - Ajoutez du SCRUM dans votre gestion de projets créatifs](https://www.youtube.com//watch?v=GNJpKoaLG7c&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=12)
-  [Martin Dupont, Richard Martin - Bureau de portefeuille Agile-Lean](https://www.youtube.com//watch?v=IJqOt_RQbio&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=13)
-  [Ali Bentaleb - Transformer votre gestion de portefeuille pour une entreprise Lean à l'aide de SAFe](https://www.youtube.com//watch?v=faW-7uGGDB0&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=14)
-  [Francois Carriere, Guillaume Soyer, Joseph Rossi - L’agilité en trois actes](https://www.youtube.com//watch?v=krAuRYJGHn0&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=15)
-  [Francois Bonetto, Nicolas Lellouche - Transformation numérique](https://www.youtube.com//watch?v=YNTP2uWbx28&list=PLKUt9pKJ0E0efpRrVxpx4BMaxYwPIACqd&index=16)


### Agile Montreal 2018
-  [Carol Alain (Keynote) - Le choc des générations, un enjeu au coeur des transformations agiles](https://www.youtube.com//watch?v=yOVlEACCIng&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=2)
-  [Joshua Kerievsky (Keynote) - Are you curious?](https://www.youtube.com//watch?v=GouzOVnkNsg&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=3)
-  [Félix-Antoine Bourbonnais - Agilité: l’art d’expérimenter](https://www.youtube.com//watch?v=0Te-2q0gAjc&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=4)
-  [Maurizio Mancini et Paul Ryan - Getting Agile right - Rebooting an Agile organization in 100 days](https://www.youtube.com//watch?v=c0K2wwGeogk&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=5)
-  [Félix-Antoine Bourbonnais - Pourquoi mes tests automatisés sont durs à maintenir?](https://www.youtube.com//watch?v=5h9U6RnLGJY&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=6)
-  [Jeff "Cheezy" Morgan et Ardita Karaj - Continuous Delivery is more than DevOps](https://www.youtube.com//watch?v=u0jkbDfg3PM&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=7)
-  [Louise Kold-Taylor et Steffan Surdek  - What Happens in Safe Space?](https://www.youtube.com//watch?v=x30qObqlL9M&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=8)
-  [Marie-Andrée Roger Paquet, Sylvain Ethier - Dév agile des produits : un chemin pour l’innovation](https://www.youtube.com//watch?v=1r6UBEtrZvs&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=9)
-  [Alain C. Courville, Olivier Bataille, Isabelle Therrien - Miser sur l’Agilité et le Design Thinking](https://www.youtube.com//watch?v=rijUcunXAFs&list=PLKUt9pKJ0E0fnGjauc1SlUxUAMhg1bNKx&index=10)


### Agile Montreal 2017
-  [Pierre Neis - An Introduction to Agile Organization](https://www.youtube.com//watch?v=FvOWmgkN-ak&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=2)
-  [Jennifer Spear - Innovation doesn’t follow a script: Work and Lead UnScripted™](https://www.youtube.com//watch?v=OKum45yS-Ok&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=3)
-  [Henry Mintzberg - listening to his current views on management, rebalancing society and agility](https://www.youtube.com//watch?v=HCA8Bvl281A&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=4)
-  [François Bonetto et Eric Dessureault - L'Agilité comme cadre de transformation d’organisation](https://www.youtube.com//watch?v=RgrQ2XwF98c&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=5)
-  [Mathieu Boisvert et Guillaume Soyer - Livre de jeux de l’Agilité à grande échelle (Agile Scaling)](https://www.youtube.com//watch?v=vlpm7HS1K-g&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=6)
-  [Isabelle Therrien - Mieux décoller pour mieux atterrir](https://www.youtube.com//watch?v=w5wL0F4A1MA&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=7)
-  [Daniel S. Vacanti - Your Project Behaves Like a Hurricane — Forecast It Like One](https://www.youtube.com//watch?v=wUyi5rb2Xvk&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=8)
-  [Patrick Gagné - Téo Taxi – La transformation d’une industrie](https://www.youtube.com//watch?v=GYBxR9qgh4M&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=9)
-  [Vikas Narula - The Power of Connection – Bridging the Divide](https://www.youtube.com//watch?v=Q1thFYprlOA&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=10)
-  [Etienne Laverdière - La gestion de portefeuille en mode agile](https://www.youtube.com//watch?v=jmIgu_mWoXo&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=11)
-  [Pierre-Majorique Léger - Les neurosciences pour évaluer l’expérience vécue par vos utilisateurs](https://www.youtube.com//watch?v=p_zXfUAagvE&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=12)
-  [Nicolas Mercier et Frédéric Paquet - Gestion de portefeuille performante et Kanban stratégique](https://www.youtube.com//watch?v=kIwYRkFqZnU&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=13)
-  [Martin Lapointe - Le redémarrage de l’entreprise Agile, La fonte de l’ère de glace!](https://www.youtube.com//watch?v=Pav-Hm9gM1E&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=14)
-  [Yan Matagne & Benoit Hermant - Nous ne savons pas combien de temps durera cette session #noestimates](https://www.youtube.com//watch?v=jToDELd5PKc&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=15)
-  [David-Alexandre Paquette - LEGO Serious Play en mode Agile, jouer sérieusement en toute Agilité](https://www.youtube.com//watch?v=VCLFxAJeT0k&list=PLKUt9pKJ0E0fTb-d-h5ZjJGpMLvRl1LFy&index=16)


### Agile Montreal 2016
-  [Rethinking Agile Transformation - Jason Little](https://www.youtube.com//watch?v=uft70aN4_Zs&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=2)
-  [Agile Everywhere! - Henrik Kniberg](https://www.youtube.com//watch?v=moKG0RQNiqM&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=3)
-  [Change Management Discussion Panel - Agile Tour Montreal 2016](https://www.youtube.com//watch?v=N0xlBRd8v9I&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=4)
-  [L'innovation à l'autre bout du monde - Simon De Baene](https://www.youtube.com//watch?v=M_ZO4gMDX9Y&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=5)
-  [Agile Isn’t Enough: Revolution over Transformation - Todd Charron](https://www.youtube.com//watch?v=qGucveKc-4c&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=6)
-  [Spécifications exécutables : Une pratique au cœur du développement Agile - Alain Chaput, Jean Godard](https://www.youtube.com//watch?v=6P8FvlYiSNo&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=7)
-  [Sociocratie et Lean Change Method - Etienne Laverdière](https://www.youtube.com//watch?v=ZInQiVqoqng&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=8)
-  [Le guide de réparation de l’équipe Agile : La recette secrète! - Martin Lapointe, Maurizio Mancini](https://www.youtube.com//watch?v=STb-qsIWv5E&list=PLKUt9pKJ0E0dlz59gMjpa9OSu8dfS9WL0&index=9)


### Agile Montreal 2015
-  [Luc Alain Giraldeau - De surprenantes leçons sur le partage de l’information](https://www.youtube.com//watch?v=S6aZ3p3szyM&list=PLKUt9pKJ0E0fzUpCwVFOhoXbKc9LMMsGb&index=1)
-  [Captain David Marquet - Create Leaders at Every Level](https://www.youtube.com//watch?v=Be7f7vDorcE&list=PLKUt9pKJ0E0fzUpCwVFOhoXbKc9LMMsGb&index=2)
-  [L’organisation Agile (Discussion)](https://www.youtube.com//watch?v=BeGKd_WwcJo&list=PLKUt9pKJ0E0fzUpCwVFOhoXbKc9LMMsGb&index=3)
-  [Ellen Grove - People are weird: how non-rational decision making will mess you up](https://www.youtube.com//watch?v=19lyIoO3tUw&list=PLKUt9pKJ0E0fzUpCwVFOhoXbKc9LMMsGb&index=4)
-  [Félix-Antoine Bourbonnais - L’excellence et le talent: le coeur de l’Agilité](https://www.youtube.com//watch?v=QwdHpZCAmfM&list=PLKUt9pKJ0E0fzUpCwVFOhoXbKc9LMMsGb&index=5)

### Agile Montreal 2014
-  [La valeur d’affaires : L’indicateur qui peut changer le succès des projets - Mathieu Boisvert](https://www.youtube.com//watch?v=UzNqogV-FJ8&list=PLKUt9pKJ0E0dVZIq_gWTiAWTZZjMI5v3d&index=2)
-  [Lessons in Gravity: What keeps you down? - David Hussman](https://www.youtube.com//watch?v=qDTtKgYolN0&list=PLKUt9pKJ0E0dVZIq_gWTiAWTZZjMI5v3d&index=3)
-  [Architecture Agile et développement durable - Félix-Antoine Bourbonnais](https://www.youtube.com//watch?v=AvMEhRwpw4U&list=PLKUt9pKJ0E0dVZIq_gWTiAWTZZjMI5v3d&index=4)
-  [Transformer le management pour réussir une transformation Agile - Jean-Marc De Jonghe](https://www.youtube.com//watch?v=jxo0jsyPm_0&list=PLKUt9pKJ0E0dVZIq_gWTiAWTZZjMI5v3d&index=5)

### Agile Montreal 2013
-  [Maxime Boilard - La quête de l'excellence](https://www.youtube.com//watch?v=sNY8sSMtvNI&list=PLKUt9pKJ0E0dwx8DP7-rx1QkVRoy3YAQC&index=2)
-  [François Beauregard - Apprendre à danser avec les polarités](https://www.youtube.com//watch?v=cHeOc5NL21s&list=PLKUt9pKJ0E0dwx8DP7-rx1QkVRoy3YAQC&index=3)
-  [Éric Laramée - Les catalyseurs à l'engagement d'une équipe Agile](https://www.youtube.com//watch?v=OhXx7_xgUUk&list=PLKUt9pKJ0E0dwx8DP7-rx1QkVRoy3YAQC&index=4)
-  [Linda Rising - Incentives: why or why not?](https://www.youtube.com//watch?v=4m--PMNMkwE&list=PLKUt9pKJ0E0dwx8DP7-rx1QkVRoy3YAQC&index=5)
